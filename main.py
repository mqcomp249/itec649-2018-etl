"""
Module to read data from CSV files and HTML file
to populate an SQL database

ITEC649 2018
"""

import csv
import sqlite3
from database import DATABASE_NAME


# define your functions to read data here


if __name__=='__main__':
    db = sqlite3.connect(DATABASE_NAME)

    # Add your 'main' code here to call your functions
